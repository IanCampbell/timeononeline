# Time On One Line
## About
This project takes up one line of a vertical taskbar, whereas the windows 10  
 clock takes up two (for me it takes up two, seems it always changes).

Also, I couldn't find how to easily change the date on the Windows 10 clock to  
 show the month (D/M/YYYY is too ambiguous. Is 1/8/2019 1 August or 8 January?)

Also I can glance at "13:00 Tuesday 29 October 2019" and absorb all the info,  
 but somehow the same info across three lines, I take three separate glances.

This Project:
![alt text](./doc/Screenshot.png "Time On One Line")

vs Default Windows 10:
![alt text](./doc/DefaultWindows10Clock.png "Default Windows 10 Clock")

## Build
1. Install Visual Studio Code
2. Install .NET Core from https://dotnet.microsoft.com/download
3. Install C# extension from the Visual Studio Code Marketplace
4. In VS Code, File > Open Folder and open this folder
5. In VS Code, View > Terminal
6. In VS Code Terminal, type "dotnet build -c Release"
7. In VS Code Terminal, type "dotnet run -c Release"
8. [Optional] right click the running Time On One Line and "Pin To Taskbar"