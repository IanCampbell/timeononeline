﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace timeononeline
{
    public partial class Form1 : Form
    {
        static System.Windows.Forms.Timer myTimer = new System.Windows.Forms.Timer();
        public Form1()
        {
            InitializeComponent();
            myTimer.Tick += new EventHandler(TimerEventProcessor);
            myTimer.Interval = 2000; //2s
            myTimer.Start();
        }

        // This is the method to run when the timer is raised.
        private void TimerEventProcessor(Object myObject, EventArgs myEventArgs)
        {
            this.Text = System.DateTime.Now.ToShortTimeString() + " " 
                        + DateTime.Now.DayOfWeek + " " 
                        + System.DateTime.Now.ToLongDateString() ;
            this.WindowState = FormWindowState.Minimized;
        }
    }
}
